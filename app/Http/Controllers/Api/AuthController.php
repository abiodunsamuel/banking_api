<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;


class AuthController extends BaseController
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);


        $user->save();


        return $this->sendResponse($user, 'User register successfully.');
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);


        $credentials = request(['email', 'password']);


        if(!Auth::attempt($credentials)){
            return $this->sendError('email or password does not match'); 
        }


        $user = $request->user();
        $token = $user->createToken('MyApp');
        $tokenTaken = $token->accessToken;
        $token->token->save();

        $success['access_token'] =  $tokenTaken;
        $success['token_type'] = 'Bearer';
        $success['expires_at'] = Carbon::parse(
                    $token->token->expires_at
                )->toDateTimeString();
        $success['name'] =  $user->name;

       
        return $this->sendResponse($success, 'User Logged in successfully.');

    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
    
        return $this->sendResponse(' ', 'User Successfully Logout out');
    }


    public function user(Request $request)
    {
        
        return $this->sendResponse($request->user(), 'User details.');
    }


}

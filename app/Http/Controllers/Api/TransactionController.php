<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Transaction;
use App\User;
use Validator;

class TransactionController extends BaseController
{
    
    public function deposit(Request $request){

        $validator = Validator::make($request->all(), [
            'amount' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $user_id = $request->user()->id;
        $userDetails = User::where('id', $user_id)->first();
        $newBalance = $userDetails->account_balance + $request->amount;

        
        $newTransaction = new Transaction;
        $newTransaction->user_id = $user_id;
        $newTransaction->amount = $request->amount;
        $newTransaction->transaction_type = 'D';

        if($newTransaction->save() == true){
            $userDetails->update(['account_balance' => $newBalance]);
            $success['amount'] = $newTransaction->amount;
            $success['transaction'] = 'Deposit';
            $success['balance'] = $userDetails->account_balance;

            return $this->sendResponse($success, 'Deposit successfully');
        }else{
            return $this->sendError('Deposit Error');
        }

    }

    public function withdrawal(Request $request){

        $validator = Validator::make($request->all(), [
            'amount' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $user_id = $request->user()->id;
        $userDetails = User::where('id', $user_id)->first();
        

        if($request->amount > $userDetails->account_balance){
            return $this->sendError('Insufficient Balance');
        }

        $newBalance = $userDetails->account_balance - $request->amount;

        $newTransaction = new Transaction;
        $newTransaction->user_id = $user_id;
        $newTransaction->amount = $request->amount;
        $newTransaction->transaction_type = 'W';

        if($newTransaction->save() == true){
            $userDetails->update(['account_balance' => $newBalance]);
            $success['amount'] = $newTransaction->amount;
            $success['transaction'] = 'Withdrawal';
            $success['balance'] = $userDetails->account_balance;

            return $this->sendResponse($success, 'Withdrawal successfully');
        }else{
            return $this->sendError('Withdrawal Error');
        }
    }

    public function balance(Request $request){

        $user_id = $request->user()->id;
        $userDetails = User::where('id', $user_id)->first();
        $success['balance'] = $userDetails->account_balance;
        
        return $this->sendResponse($success, 'Account balance genarated');
    }

}

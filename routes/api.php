<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::post('register', 'Api\RegisterController@register');
// Route::middleware('auth:api')->post('withdrawal', 'Api\TransactionController@performWithdrawal');
// Route::middleware('auth:api')->post('deposit', 'Api\TransactionController@performDeposit');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
    });
});


Route::group([
    'prefix' => 'transaction',
    'middleware' => 'auth:api'
], function () {
    Route::post('deposit', 'Api\TransactionController@deposit');
    Route::post('withdrawal', 'Api\TransactionController@withdrawal');
    Route::get('balance', 'Api\TransactionController@balance');
  
   
});



